#include <iostream>
#include <map>
#include "WordHistogram.h"


int main(int argc, char* argv[]) {
  
  if(argc < 2 || argc> 2) {
    std::cerr << "INVALID PARAMETERS :: TRY AGAIN\n";
    exit(0);
  }

  // arg[1] is the names of the file.
  std::string filename = argv[1];
  WordHistogram *WH = new WordHistogram(filename);

  // Create and fill the map
  WH -> mapFile(filename);
  // Display the map to the console
  WH -> display();
}
