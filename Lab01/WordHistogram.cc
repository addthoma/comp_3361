//WordHistory.cc
//Addison Thomas
//19 Sept. 2019

#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include "WordHistogram.h"

WordHistogram::WordHistogram(std::string filename) {
  // Instantiate the words listing
  uniqueWords = 0;
  totalWords = 0;
}

// Function creates the map by opening the parameter file and using
// the words as keys. 
void WordHistogram::mapFile(std::string filename) {
  std::string file = filename;
  std::cout << "Opening File:: " + file << "\n";
  // Create the stream
  std::ifstream ifs (file);
  
  if(!ifs.is_open()) {
    std::cerr << "File does not exist. Input an existing file\n";
    exit(0);
  }

  std::string line;
  // Iterate through the file
  while(getline(ifs,line)) {
    std::istringstream stream(line);
    std::string word;
    while(getline(stream,word,' ')) {
      // Add the words to the map
       if(wordMap.count(word)) {
	 wordMap[word] = wordMap[word] + 1;
	 totalWords += 1;
       } else {
	 wordMap.insert({word,1});
	 uniqueWords += 1;
	 totalWords += 1;
       }
    }
  }
  ifs.close();
}

// Displays results to the console
void WordHistogram::display() {
  std::cout << "Number of words read: " << getCount() << '\n';
  std::cout << "Number of unique words read: " << getUniqueCount() << '\n';
  std::cout << "Histogram:\n";

  for(auto& i : wordMap) {
    std::cout << i.first << ": " << i.second << '\n';
  }
}


//// Accessors ////
int WordHistogram::getCount() {
  return totalWords;
}

int WordHistogram::getUniqueCount() {
  return uniqueWords;
}



