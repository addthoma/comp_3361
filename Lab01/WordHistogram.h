//WordHistory.h
#include<iostream>
#include<map>

// WordHistogram is intended to map words into buckets and keep
// a count of how many words there are. 
class WordHistogram {
 private:
   int uniqueWords;
   int totalWords;
   std::map<std::string,int> wordMap;
 public:
   WordHistogram(std::string filename);
   void mapFile(std::string filename);
   int getCount();
   int getUniqueCount();
   void  display();
};
