// Main.c
// Addison Thomas
// 19 Sept. 2019

#include <stdio.h>
#include<pthread.h>
#include<stdbool.h>
#include<stdlib.h>

pthread_mutex_t intlock;
int numPrimes = 0;

bool isPrime(int n) {
  if(n==1){
    return true;
  }
  if(n==2) {
    return false;
  }
  int i = 2;
  while(i < n/2) {
    if(n%i==0) {
      return false;
    }
    i++;
  }
  return true;
}


void* findPrimes(void* ints) {
  int* temp = (int*)ints;
  
  int start = temp[0];
  int end = temp[1];

  printf("Start: %i\nEnd: %i\n",start,end);
  pthread_mutex_lock(&intlock);
  int localprimes = 0;
  for(int i=start;i<end;i++){
    if(isPrime(i)) {
      printf("%i\n",i);
      
      numPrimes+=1;
      localprimes += 1;
    } 
  }
  pthread_mutex_unlock(&intlock);
  free(ints);
}



int main(int argc, char* argv[]) {

  int numthreads = 1;

  pthread_t *tid = malloc( numthreads * sizeof(pthread_t) );
  int start = 10;
  int end = 100;
  
  if(argc>2) {
    start = atoi(argv[1]);
    end = atoi(argv[2]);
  }
  if(argc>3) {
    numthreads = atoi(argv[3]);
  }

  int diff = end - start;
  int increment = diff/numthreads;
  
  int threadstart = start;
  int threadend = start + increment;
  
  
  printf("threadstart %i, threadend %i\n",threadstart,threadend);
  printf("increment %i, diff %i",increment, diff);
  for(int i=0;i<numthreads;i++) {
    int* intpoint = (int*)malloc(2 * sizeof(int));
    printf( "Thread %i is reading from %i to %i\n",i,threadstart,threadend);
    intpoint[0] = threadstart;
    intpoint[1] = threadend;
    printf("Start: %i, End: %i",threadstart,threadend);
    pthread_create(&tid[i], NULL, findPrimes, intpoint);

    threadstart = threadstart + increment;
    threadend = threadend + increment;
  }
  
  for (int i=0; i<numthreads; i++) {
    pthread_join(tid[i],NULL);
  }

  
  printf( "Number of primes from %i to  %i is:  %i\n",start,end,numPrimes);
}
 
