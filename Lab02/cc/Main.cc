// Main.cc
// Addison Thomas
// 19 Sept. 2019

#include <iostream>
#include "PrimeFinder.h"
#include <pthread.h>
#include<thread>

void statFind(PrimeFinder* p, int a, int b, int thread) {
  int local = p->findPrimes(a,b);
  std::cout << "Thread " << thread << ": " << local  << '\n';
}

int main(int argc, char* argv[]) {

  PrimeFinder* pf = new PrimeFinder();
  int numthreads = 4;
  int start = 1;
  int end = 100;
  
  if(argc>2) {
    start = atoi(argv[1]);
    end = atoi(argv[2]);
  }
  if(argc>3) {
    numthreads = atoi(argv[3]);
  }

  int diff = end - start;
  
  int increment = diff/numthreads;
  std::thread* threads[numthreads];
  pthread_t threadstart = start;
  pthread_t threadend = start + increment;
  
  for(int i=0;i<numthreads;i++) {
    std::cout << "Thread " << i << " is reading from " << threadstart << " to " << threadend <<'\n';
    threads[i] = new std::thread(statFind,pf,threadstart,threadend,i);
    threadstart+=increment;
    threadend+=increment;
  }
  
  for (int i=0; i<numthreads; i++) {
   threads[i]->join();
  }

  int primes = pf->getPrimes();
  std::cout << "Number of primes from " << start << " to " << end << " is: " << primes << '\n';
}
 
