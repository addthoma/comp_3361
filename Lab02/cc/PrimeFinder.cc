// PrimeFinder.cc
// Addison Thomas
// 19 Sept. 2019

#include <iostream>
#include "PrimeFinder.h"
#include <vector>
#include<thread>
#include <mutex>

PrimeFinder::PrimeFinder() {
  numPrimes = 0;
}

bool PrimeFinder::isPrime(int n) {
  if(n==1){
    return true;
  }
  if(n==2) {
    return false;
  }
  int i = 2;
  while(i < n/2) {
    if(n%i==0) {
      return false;
    }
    i++;
  }
  return true;
}

int PrimeFinder::findPrimes(int start, int end) {
  int localprimes = 0;
  for(int i=start;i<=end;i++){
    if(isPrime(i)) {
      intlock.lock();
      numPrimes+=1;
      intlock.unlock();
      localprimes += 1;
    }
  }
  return localprimes;
}

int PrimeFinder::getPrimes() {
  return numPrimes;
}
