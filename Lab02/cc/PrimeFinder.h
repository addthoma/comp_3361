// PrimeFinder.h
// Addison Thomas
// 19 Sept. 2019

#include <iostream>
#include<vector>
#include <mutex>

class PrimeFinder {
 private:
  int numPrimes;
  std::vector<int>* primes;
  std::mutex intlock;

  
 public:
  PrimeFinder();
  bool isPrime(int i);
  int findPrimes(int start, int end);
  int getPrimes();
};
