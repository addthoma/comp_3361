// Pool.c
// Addison Thomas
// 7 Oct. 2019

#include <stdio.h>
#include "queue.h"
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define QSIZE  255
int threads;
bool stop = false;
queue* work;
pthread_t *tid;
int * fin;
pthread_mutex_t queuelock;
pthread_cond_t cond;


void workerThread(void* id) {
    int threadid = (int) id;
    
    //While dont stop, do the work
    while(!stop) {
	
	pthread_mutex_lock(&queuelock);
	
	while(!QisEmpty(work)) {
	      fin[threadid] = 0;
	      int workint = Qremove(work);
              printf("Worker %i doing work %i\n",threadid,workint);
              pthread_mutex_unlock(&queuelock);
              sleep(workint);
	      printf("Worker %i done doing work %i\n",threadid,workint);
        }
	
	
	fin[threadid] = 3;
	printf("Worker %i going to sleep\n",threadid);
	pthread_cond_wait(&cond,&queuelock);
	printf("Threads %i woken back up\n",threadid);
	pthread_mutex_unlock(&queuelock);
    }
    //After the work on the queue has been done, change state to stop state.
    printf("Worker %i exiting\n",threadid);
}

void createPool(int numthreads) {
  //craete the threads
    tid = malloc( numthreads * sizeof(pthread_t) );
    for(int i = 0;i<numthreads;i++) {
        pthread_create(&tid[i], NULL, workerThread,(void*)i);
    }
}



void addWork(int n) {
  
  int rnum = (rand() % 11) + 1; 		       
  printf("--> Adding work %i\n",rnum);
  //lock the queue and insert the work onto the queue
    pthread_mutex_lock(&queuelock);
    Qinsert(work,rnum);
    pthread_mutex_unlock(&queuelock);
   
    //find the first sleeping thread and tell it to do the work
    pthread_cond_signal(&cond);
}

void stopPool(int numthreads) {
    //tell all the threads to stop by changing the global var.
  bool done = false;
  int  magicnum = 3*numthreads;
  while(!done) {
    int sum = 0;
    for(int i = 0;i<numthreads;i++) {
      sum+=fin[i];
    }
    if(sum==magicnum) {
      done = true;
    }
  }
  printf("Queue is empty, threads are done\n");
    stop = true;
    pthread_cond_broadcast(&cond);
    //join the threads together.
    for(int i=0;i<numthreads;i++) {
      pthread_join(tid[i],NULL);
    }  
}

int main(int argc,char* argv[]) {
  //initialize number of threads and create the work queue
    threads = 4;
    int ammwork = 10;

    
    time_t t;
    srand(time(&t));

    fin = (int*)malloc(threads*sizeof(int));
    for(int i = 0;i<threads;i++) {
      fin[i] = 0;
    }
    int numthreads = threads;
    work = Qcreate(QSIZE);
    //STATE is for looking up state of 
    createPool(numthreads); // Start up the pool with 4 worker threads
    for (int i=0; i<ammwork; i++) {   // Add 2 (or later 10) items of work
        addWork(i);
    }
    stopPool(numthreads); // This will stop the pool once the work in the queue is finished
    // sleep(60);  // This can be used to keep the main alive until you finish coding stopPool
}
