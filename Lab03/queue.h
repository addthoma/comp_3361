#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct queue {
  int* elements;  // array of elements (currently ints)
  int max;
  int front;
  int rear;
  int itemCount;
} queue;

queue* Qcreate(int size) {
  queue* q = (queue*)malloc(1 * sizeof(queue));
  q->elements = (int*)malloc(size * sizeof(int));
  q->max = size;
  q->front = 0;
  q->rear = -1;
  q->itemCount = 0;
  return q;
}

int Qpeek(queue *q) {
  return q->elements[q->front];
}

bool QisEmpty(queue *q) {
  return q->itemCount == 0;
}

bool QisFull(queue *q) {
  return q->itemCount == q->max;
}

int Qsize(queue *q) {
  return q->itemCount;
} 

void Qinsert(queue *q, int data) {
  if(!QisFull(q)) {    
    if(q->rear == q->max - 1) {
      q->rear = -1;            
    }       
    q->elements[++q->rear] = data;
    q->itemCount++;
  }
}

int Qremove(queue *q) {
  int data = q->elements[q->front++];
  if(q->front == q->max) {
    q->front = 0;
  }  
  q->itemCount--;
  return data; 
}
