//Main.c
//addison Thomas
//10/21/19
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
int TOTALRESOURCE;
int MAX = 255;

int main(int argc, char*argv[]) {
  
  if(argc < 2) {
    printf("INCLUDE FILENAME: EXITING\n");
    exit(0);
  }

  
  char* buff = malloc(MAX * sizeof(char));
  FILE *fp = fopen(argv[1],"r");
  
  
  if (fp == NULL){
    perror ("Error opening file\n");
  }
   else {
     
     fgets(buff,255, fp);
     printf("BUFF: %s\n",buff);
     
     TOTALRESOURCE = atoi(buff);
     int numentries = 0;
     int avail[TOTALRESOURCE];
     char* names[MAX];
     int index = 0;
     int line = 0;
     int assign[MAX][TOTALRESOURCE];
     int required[MAX][TOTALRESOURCE];
     
      //--------------------------------- READ THE FILE INTO ARRAYS ------------------------------
     fgets(buff,255,fp);
     avail[0] = atoi(strtok(buff," "));
     
     for(int i = 1;i<TOTALRESOURCE;i++) {
       avail[i] = atoi(strtok(NULL," "));
     }
    
     while(fgets(buff,255,fp) != NULL) {
       
       printf("BUFF: %s",buff);
       names[index] = strtok(buff," ");
       printf("NAME: %s at index %i\n",names[index],index);
       index += 1;
       for(int i = 0; i<TOTALRESOURCE; i++) {
	  assign[line][i] = atoi(strtok(NULL," "));
       }
        for(int i = 0; i <numentries;i++) {
	 printf( "Names at %i: %s\n",i,names[i]);
       }
        for(int i = 0;i<TOTALRESOURCE;i++){
	  // printf("ASSIGN at% i: %i\n",i,assign[i]);
	  required[line][i] = atoi(strtok(NULL," "));
       }

	numentries++;
	line++;
	buff = malloc(MAX*sizeof(char));
	// for(int i = 0;i<TOTALRESOURCE;i++){
	   // printf("REQUIRED at% i: %i\n",i,required[i]);
	// }
	 //-------------------------------------------------------------------------------------
     }
     int marked[numentries];
     
     for(int i = 0; i<numentries;i++) {
       marked[i] = 0;
     }

     
     int magicnum = numentries;
     int curr = 0;
     while(curr != magicnum) {
       bool safe = false;
       printf("\navail:");
       for(int i = 0; i<TOTALRESOURCE;i++) {
	    printf(" %i",avail[i]);
       }
       printf("\n");	      
       //iterate over the entries in the file
      
       
       for(int i = 0; i< numentries;i++) {
      
	 printf("%s :has ",names[i]);
	   
	 for(int j=0;j<TOTALRESOURCE;j++) {
	   printf(" %i ",assign[i][j]);
	 }
	 printf(",max ");
	 for(int j = 0; j <TOTALRESOURCE;j++) {
	   printf(" %i ",required[i][j]);
	 }
	 printf(",marked: %i\n",marked[i]);
	 
	 //if its marked, skip
	 if(marked[i]) {
	 } else {
	   
	   bool able = true;
	   //iterate over the resources
	   for(int j = 0; j <TOTALRESOURCE;j++) {
	     //if the resources are availible, continue on, if not, we need to skip this resource and try the next one.
	     if((avail[j] + assign[i][j]) >= required[i][j]) {
	     }else {
	       able = false;
	     }
	     
	   }
	   if(able) {
	     printf("Schedule %s next.\n",names[i]);
	     for(int j = 0; j<TOTALRESOURCE; j++) {
	       avail[j] += assign[i][j];
	       assign[i][j] = 0;
	     }
	     marked[i] = 1;
	     curr += 1;
	     
	     safe = true;
	     break;
	   }
	   
	 }
       }

       if(!safe) {
	 printf("Not Safe, exiting");
	 exit(0);
       }
     }

     printf("Safe!");
   }
}
