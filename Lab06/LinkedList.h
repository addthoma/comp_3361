//LinkedList.h
//Addison Thomas
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct Node {
  int base;
  int size;
  char* type;
  struct Node* prev;
  struct Node* next;
  
} Node;

typedef struct list {
  Node* head;
  Node* tail;
} List;

List* initList(int size) {
  List* ret = (List*)malloc(sizeof(List));
  ret->head = (Node*)malloc(sizeof(Node));
  // ret->head->tail = NULL;
  ret->head->type = NULL;
 
  ret->tail = (Node*)malloc(sizeof(Node));
  ret->tail->next = NULL;
  ret->tail->type = NULL;
  
  Node* first = (Node*)malloc(sizeof(Node));
  first->prev = ret->head;
  first->next = ret->tail;
  first->base = 0;
  first->size = size;
  first->type = "HOLE";
  ret->head->next = first;
  ret->tail->prev = first;

  return ret;
}

void insertBefore(List* list,Node* parent,int mem,char* name) {
  Node* addition = (Node*)malloc(sizeof(Node));

  addition->type = name;
  addition->size = mem;
  if(parent->prev == list->head) {
    addition->base = 0;
  }else{
    addition->base = parent->base;
  }
  addition->prev = parent->prev;
  addition->next = parent;
  parent->prev->next = addition;
  parent->prev = addition;
  parent->base += addition->size;
  parent->size -= addition->size;

  if(parent->size == 0) {
    addition->next = parent->next;
    parent->next->prev = addition;
    free(parent);
  }
  
    
}

void printList(List* list) {
  Node* head = list->head;
  while(head->next!=list->tail) {
    head = head->next;
    printf("%s: Base %i, Size: %i\n",head->type,head->base,head->size);
  }
}
bool isEmpty(List* list) {
  return list->head->next == list->tail;
}

Node* findFirst(List* list,int mem,char* name) {
   if(isEmpty(list)) {
     printf("LIST IS EMPTY\n");
    return NULL;
   }

  Node* temp = list->head->next;
  while(temp != list->tail){
    
    if(strcmp(temp->type,name) == 0 && temp->size >= mem) {
      return temp;
    }
    temp = temp->next;
  }
  printf("NOT FOUND: %s\n",name);
  // printList(list);
  return NULL;
}

Node* findWorst(List* list, int mem,char* name) {
   if(isEmpty(list)) {
     printf("LIST IS EMPTY\n");
    return NULL;
   }

  Node* temp = list->head->next;
  int size = 0;
  Node* worst = NULL;
  while(temp != list->tail){
    
    if(strcmp(temp->type,name) == 0 && temp->size >= mem && temp->size >= size) {
      worst = temp;
      size = temp->size;
    }
    temp = temp->next;
  }
  // printf("NOT FOUND: %s\n",name);
  // printList(list);
  return worst;
}

Node* findBest(List* list, int mem, char* name) {
   if(isEmpty(list)) {
     printf("LIST IS EMPTY\n");
    return NULL;
   }

  Node* temp = list->head->next;
  int best = 10000000;
  Node* ret = NULL;
  while(temp != list->tail){
    
    if(strcmp(temp->type,name) == 0 && temp->size >= mem && temp->size < best) {
      ret = temp;
      best = temp->size;
    }
    temp = temp->next;
  }
  // printf("NOT FOUND: %s\n",name);
  // printList(list);
  return ret;
}


void unload(List* list,Node* item) {
  if(item!=NULL) {
    item->type = "HOLE";
  
    if(item->prev!=NULL && item->prev->type == "HOLE") {
      int size = item->size;
      Node* next = item->next;
      Node* prev = item->prev;
      prev->size+=size;
      prev->next = next;
      next->prev = prev;
      item = prev;
    }
    if(item->next!=NULL && item->next->type == "HOLE") {
      int size = item->size;
      Node* prev = item->prev;
      Node* next = item->next;
      next->size += size;
      next->base = item->base;
      next->prev = prev;
      prev->next = next;
      item = next;
    }
  } else {
    printf("Node Not Loaded\n");
  }
}

void firstFit(List* list, char* buff, FILE* fp, int MAX) {
    while(fgets(buff,255,fp)) {
    
    printf("%s",buff);
    
    char* instruction = strtok(buff," ");
   
    
    if(strcmp(instruction, "load")==0) {
      
      char* name = strtok(NULL," ");
      int mem = atoi(strtok(NULL," "));
      
      Node* firsthole = findFirst(list,mem,"HOLE");
      if(firsthole == NULL) {
	printf("LIST IS FULL\n");
      } else {
	insertBefore(list,firsthole,mem,name);
	printList(list);
	printf("\n");
      }
      
     
       buff = (char*)malloc(MAX*sizeof(char));

    } else if(strcmp(instruction,"unload")==0) {
       
       char* name = strtok(NULL," ");
       int mem = -1;
       Node* item = findFirst(list,mem,name);
       unload(list,item);
       
       printList(list);
       printf("\n");
       buff = (char*)malloc(MAX*sizeof(char));

    } else {
      printf("UNKNOWN COMMAND");
    }
  }
}

void bestFit(List* list,char* buff,FILE* fp,int MAX) {
      while(fgets(buff,255,fp)) {
    
    printf("%s",buff);
    
    char* instruction = strtok(buff," ");
   
    
    if(strcmp(instruction, "load")==0) {
      
      char* name = strtok(NULL," ");
      int mem = atoi(strtok(NULL," "));
      
      Node* firsthole = findBest(list,mem,"HOLE");
      if(firsthole == NULL) {
	printf("LIST IS FULL\n");
      } else {
	insertBefore(list,firsthole,mem,name);
	printList(list);
	printf("\n");
      }
      
     
       buff = (char*)malloc(MAX*sizeof(char));

    } else if(strcmp(instruction,"unload")==0) {
       
       char* name = strtok(NULL," ");
       int mem = -1;
       Node* item = findFirst(list,mem,name);
       unload(list,item);
       
       printList(list);
       printf("\n");
       buff = (char*)malloc(MAX*sizeof(char));

    } else {
      printf("UNKNOWN COMMAND");
    }
  }
}

void worstFit(List* list,char* buff,FILE* fp,int MAX) {
    while(fgets(buff,255,fp)) {
    
    printf("%s",buff);
    
    char* instruction = strtok(buff," ");
   
    
    if(strcmp(instruction, "load")==0) {
      
      char* name = strtok(NULL," ");
      int mem = atoi(strtok(NULL," "));
      
      Node* firsthole = findWorst(list,mem,"HOLE");
      if(firsthole == NULL) {
	printf("LIST IS FULL\n");
      } else {
	insertBefore(list,firsthole,mem,name);
	printList(list);
	printf("\n");
      }
      
     
       buff = (char*)malloc(MAX*sizeof(char));

    } else if(strcmp(instruction,"unload")==0) {
       
       char* name = strtok(NULL," ");
       int mem = -1;
       Node* item = findFirst(list,mem,name);
       unload(list,item);
       
       printList(list);
       printf("\n");
       buff = (char*)malloc(MAX*sizeof(char));

    } else {
      printf("UNKNOWN COMMAND");
    }
  }
}
