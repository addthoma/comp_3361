//main.c
//Addison Thomas
//Oct 27
#include "LinkedList.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


int MAX = 255;

int main(int argc,char* argv[]) {
  
  
  
  if(argc<2) {
    perror("INVALID ENTRY");
    exit(0);
  }
  FILE* fp = fopen(argv[1],"r");

  if(fp == NULL) {
    perror("ERROR READING FILE");
    exit(0);
  }
  char* buff = (char*)malloc(MAX*sizeof(char));
  fgets(buff,255,fp);
  printf("%s\n",buff);
  char* com = strtok(buff," ");
  int totalspace = atoi(strtok(NULL, " "));

  List* list = initList(totalspace);
  
  // printList(list);
  
  buff = (char*)malloc(MAX*sizeof(char));
  
  if(strcmp(com,"firstFit")==0) {
      firstFit(list,buff,fp,MAX);
  }else if(strcmp(com,"worstFit")==0) {
    worstFit(list,buff,fp,MAX);
  }else if(strcmp(com,"bestFit")==0) {
    bestFit(list,buff,fp,MAX);
  } else{
    printf("Unrecognized command");
  }
 
}
