//Main.c
//Addison Thomas
//26 Sept. 2019

#include<stdio.h>
#include<pthread.h>
#include<stdbool.h>
#include<stdlib.h>
#include<unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

char* PREVCOMS[25];

void push() {
  for(int i=24;i>1;i--) {
    PREVCOMS[i] = PREVCOMS[i-1];
  }
}

void store(char* entry) {
  push();
  PREVCOMS[0] = entry;
}

char* changecommand(int past) {
  printf("%s",PREVCOMS[past]);
  return PREVCOMS[past];
}


//Called in SIGCHLD
void handler(int sig) {
  while (waitpid((pid_t)(-1), 0, WNOHANG) > 0) {}
  }



// exit command
void exitf(char* com[]) {
  exit(0);
}

// Change directory system call
 void cd(char* com[]) {
   chdir(com[1]);
 }


//Runs any command that is passed into it.
void run(char* com[],char* input,char* output,bool background) {
  if(com[0]!=NULL) {
    
    if(strcmp(com[0],"exit") == 0) {
       exitf(com);
     }
    
     else if(strcmp(com[0],"cd") == 0) {
       cd(com);
     }

     //If the command has not been implemented, run it in the system.
     else {
       signal(SIGCHLD, handler);
       //fork the process and run the command in the child.
       int pid = fork();

       //child
       if (pid == 0) {
         int mypid = getpid();

	 //If there is a redirect, change the stdin or stdout
         if(input != NULL) {
	    freopen(input,"r",stdin);
         }
         if(output != NULL) {
	    freopen(output,"w",stdout);
         }

	 //Execute the program
	 int rc = execvp(com[0], com);
       }
       //parent
       else {
	 int status;
	 if(!background){
	    int result = waitpid(pid, &status, 0);
	 }
       }
     }
  }
}

int main(int argc, char* argv[]) {
  for(int i=0;i<25;i++){
      PREVCOMS[i] = NULL;
    }
  bool exit = false;
  //infinite loop
  while(!exit) {

  //print current working directory
  char cwd[256];
  getcwd(cwd, sizeof(cwd));
  printf("%s>",cwd);
  
  char com[256];
  gets(com);
  /*
  if(com[0] == '!') {
      com[0] = com[1];
      if(com[2] != '\0') {
	com[1] = com[2];
	com[2] = '\0';
      }else {
	com[1] = '\0';
      }
       int cngcom = atoi(com);
       *com = changecommand(cngcom);
  } else {
    store(com);
  }
  */
  
  //fgets(com,256,stdin);
  
  int init_size = strlen(com);
  char delim[] = " ";
  
  int pos = 0;

  char* commands[init_size];
  char* input=NULL;
  char* output=NULL;
  commands[0] = NULL;
  
    
  //init first token
  char *ptr = strtok(com, delim);
  
  
  bool background = false;
  while(ptr != NULL)
  {
    //Check for Redirection and Background task delimiters
      if(strcmp(ptr,"<") == 0) {
        ptr = strtok(NULL,delim);
        input = ptr;
	ptr = strtok(NULL,delim);
      }
      if(ptr != NULL) {
         if(strcmp(ptr,">") == 0) {
           ptr = strtok(NULL,delim);
	   output = ptr;
	   ptr = strtok(NULL,delim);
         }
      }
      if(ptr != NULL) {
	if(strcmp(ptr,"&") == 0) {
	  ptr = strtok(NULL,delim);
	  background = true;
	}
      }
     commands[pos] = ptr;
     ptr = strtok(NULL, delim);
     pos++;
  }
  //End each command array with a NULL
  commands[pos] = NULL;
  
  // printf("%s",strcom);
  //Run the command with parameters.
  run(commands,input,output,background);
  }
}
