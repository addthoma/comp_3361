//main.c
//addison thomas
//Oct. 29 2019

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "priorQueue.h"

//Global Settings
char* filename = "";
int block_duration = 20;
int time_slice = 10;
int MAX = 255;
int SYS_TIME = 0;
int numproc=0;

int main(int argc, char* argv[]) {
  // Get the settings from the Command line
  if(argc > 1) {
    filename = argv[1];
  }
  if(argc > 2) {
    block_duration = atoi(argv[2]);
  }
  if(argc > 3) {
    time_slice = atoi(argv[3]);
  }

  // Open the file
  FILE* fp = fopen(filename,"r");

  if(fp == NULL) {
    perror ("ERROR opening file");
    exit(0);
  }

  //Create each of the Queues and lists
  char* buff = malloc(MAX * sizeof(char));
  
  Node* workQueue = initHead();
  // Node* blockQueue = initHead();
  Entry* blocked[MAX];
  Entry* processes[MAX];
  Entry* complete[MAX];
  int index = 0;

  for(int i = 0; i<MAX;i++) {
    blocked[i] = NULL;
  }
  
  //Read the file into the processes
  while(fgets(buff,MAX,fp) != NULL) {
    
    char* name = strtok(buff," ");
    int prior = atoi(strtok(NULL, " "));
    int arrive = atoi(strtok(NULL, " "));
    int total = atoi(strtok(NULL, " "));
    int block = atoi(strtok(NULL, " "));

    Entry* queueItem = malloc(sizeof(Entry));
    
    queueItem->name = name;
    queueItem->priority = prior;
    queueItem->arrival_time = arrive;
    queueItem->total_time = total;
    queueItem->block_interval = block;
    queueItem->block_temp = block;
    queueItem->block_timer = block;
    
    processes[index] = queueItem;
    complete[index] = queueItem;
    
    index++;
    numproc++;
    printEntry(queueItem);

    buff = malloc(MAX*sizeof(char));
  }

  //End the list with a NULL for checks later.
  
  processes[index] = NULL;
  complete[index] = NULL;
  int termtime[index];
  for(int i =0; i<index;i++) {
    termtime[i] = 0;
  }
  printf("%i %i\n",block_duration,time_slice);
  
  // Run the processes until everything is complete.
  while(!isListEmpty(complete)) {
    
    printf("%i  ",SYS_TIME);
    
    //unblock and add the work to the WorkQueue
    unblock(blocked,workQueue,time_slice,block_duration);
    addWork(processes,workQueue,SYS_TIME);
    
    //If workQueue is empty either everything is done or blocking
     if(isEmpty(&workQueue)) {
       Entry* leastblocked;
       int addtime = MAX;
       index = 0;
       for(int i = 0;blocked[i] != NULL;i++) {
	 // printf("BLOCKTIMER: %i %s ",blocked[i]->block_timer,blocked[i]->name);
	 
	 if(blocked[i]->block_timer < addtime) {
	   leastblocked = blocked[i];
	   addtime = blocked[i]->block_timer;
	   index = i;
	 }
	 
       }
       // printf(" ||| ");
       for(int i = 0;blocked[i] != NULL;i++) {
	 blocked[i]->block_timer -= addtime;
       }
       for(int i = 0;blocked[i] != NULL;i++) {
	 if(blocked[i]->block_timer<=0) {
	   blocked[i]->block_timer = block_duration;
	   push(&workQueue,blocked[i],blocked[i]->priority);
	   for(int j = i;blocked[j]!=NULL;j++) {
	     blocked[j] = blocked[j+1];
	   }
	   i--;
	 }
       }
       
       
       printf("(IDLE) %i I ",addtime);
       SYS_TIME+=addtime;
       
     } else {
       
       //Get the first item on the queue.
       Entry* temp = peek(workQueue);
       pop(workQueue);
       
       //Print the name
       printf("%s ", temp->name);
       
       //Work on it.
       temp->total_time -= time_slice;
       temp->block_temp -= time_slice;
       
       //Determine if it needs to block, if it is complete,
       // or if it will be added back to the workqueue.
       
       if(temp->total_time <= 0) {
	 
	 completeNode(temp,complete);
	 printf("%i T",(time_slice + temp->total_time));
	 for(int i = 0; i<numproc;i++) {
	   if(termtime[i]==0) {
	     termtime[i]=SYS_TIME+(time_slice+temp->total_time);
	     break;
	   }
	 }
	 SYS_TIME+=(time_slice+temp->total_time);
	 
       } else {
	 
	 if(temp->block_temp <= 0) {
	   
	   printf("%i B",(time_slice+temp->block_temp));
	   SYS_TIME+= (time_slice+temp->block_temp);
	   temp->block_temp = temp->block_interval;
	   // push(&workQueue,temp,temp->priority);
	   block(blocked,temp);
	   // push(&blockQueue,temp,temp->priority);
	   
	 }else {
	   
	   printf("%i S",(time_slice));
	   SYS_TIME+=time_slice;
	   push(&workQueue,temp,temp->priority);
	   
	 }
       }
     }
     printf("\n");
     // printList(complete);
     // SYS_TIME+=time_slice;
  }
  int tot=0;
  for(int i = 0;i<numproc;i++){
   tot+= termtime[i];
  }
  tot = tot/numproc;
  
  printf("%i (END) %i \n",SYS_TIME,tot);
}
