//priority queue
//Found on GeeksforGeeks
//addison thomas
//Oct 29 2019

// C code to implement Priority Queue 
// using Linked List 
#include <stdio.h> 
#include <stdlib.h> 

typedef struct entry {
  char* name;
  int priority;
  int arrival_time;
  int total_time;
  int block_interval;
  int block_temp;
  int block_timer;
} Entry;

// Node 
typedef struct node { 
    Entry* data; 
  
    // Lower values indicate higher priority 
    int priority; 
  
    struct node* next; 
  
} Node; 

Node* initHead() {
  Node* temp = (Node*)malloc(sizeof(Node));
  temp->next = NULL;
  
  return temp;
}

// Function to Create A New Node 
Node* newNode(Entry* d, int p) 
{ 
    Node* temp = (Node*)malloc(sizeof(Node)); 
    temp->data = d; 
    temp->priority = p; 
    temp->next = NULL; 
  
    return temp; 
} 
  
// Return the value at head 
Entry* peek(Node* head) 
{ 
    return (head)->next->data; 
} 
  
// Removes the element with the 
// highest priority form the list 
void pop(Node* head) 
{ 
  Node* temp = (head)->next; 
    (head)->next = (head)->next->next;
    free(temp); 
} 
  
// Function to push according to priority 
void push(Node** head, Entry* d, int p) 
{ 
  Node* start = (*head);
  
  
    // Create new Node 
    Node* temp = newNode(d, p); 
  
    // Special Case: The head of list has lesser 
    // priority than new node. So insert new 
    // node before head node and change head node. 
  
        // Traverse the list and find a 
        // position to insert new node 
        while (start->next != NULL && 
               start->next->priority > p) { 
            start = start->next; 
        } 
  
        // Either at the ends of the list 
        // or at required position 
        temp->next = start->next; 
        start->next = temp; 
  
} 
  
// Function to check is list is empty 
int isEmpty(Node** head) 
{ 
    return (*head)->next == NULL; 
}

int isListEmpty(Entry** list) {
  return list[0] == NULL;
}

void printEntry(Entry* e) {
  if(e == NULL) {
    printf("ENTRY is NULL");
  } else {
  printf("NAME: %s \n",e->name);
  printf("Priority: %i \n",e->priority);
  printf("Arrival: %i \n",e->arrival_time);
  printf("Total: %i \n",e->total_time);
  printf("Block: %i\n\n",e->block_interval);
  }
}

void printList(Entry* list[]) {
  for(int i =0;list[i] != NULL;i++) {
    printf("%s  ",list[i]->name);
  }
}

void addWork(Entry* avail[], Node* head,int time) {
  for(int i = 0; avail[i] != NULL;i++) {
    if(avail[i]->arrival_time <= time) {
      push(&head,avail[i],avail[i]->priority);
      // printf("ADDING: %s\n",avail[i]->name);
      for(int j = i;avail[j]!=NULL;j++) {
	avail[j] = avail[j+1];
      }
      i--;
    }
  }
}

void completeNode(Entry* comnode, Entry* completed[]) {
 
  for(int i = 0; completed[i] != NULL; i++ ) {
    if(strcmp(completed[i]->name, comnode->name) == 0) {
      for(int j = i; completed[j]!=NULL;j++) {
	completed[j] = completed[j+1];
      }
      i--;
    }
  }
}




void block(Entry* blocked[],Entry* newblock) {
  int index = 0;
  while(blocked[index] != NULL) {
    index++;
  }
  blocked[index] = newblock;
}


void unblock(Entry* blocked[],Node* workQueue,int time_slice,int duration) {
  for(int i = 0; blocked[i] !=NULL;i++) {
    if(blocked[i]->block_timer <= 0) {
      blocked[i]->block_timer = duration;
      push(&workQueue,blocked[i],blocked[i]->priority);
      for(int j = i;blocked[j]!=NULL;j++) {
	blocked[j] = blocked[j+1];
      }
      i--;
    } else {
      blocked[i]->block_timer -= time_slice;
    }
  }
 
}
