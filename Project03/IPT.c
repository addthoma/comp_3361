//IPT.c
//Addison Thomas
// 11 Nov. 2019
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "IPTpagehelp.h"
#include <stdint.h>
#include <math.h>

int main(int argc, char* argv[]) {
  
  if(argc < 2) {
    printf("Incorrect number of inputs. Please include input filename\n");
    exit(0);
  }
  // Open the File
  FILE* infile = fopen(argv[1],"r");
  
  if(infile == NULL) {
    printf("Error opening file; exiting.\n");
    exit(0);
  }
  
  //Buffer to read into 
  char* buff = (char*)malloc(sizeof(char*) * 255);
  fgets(buff,255,infile);
  //Parameters given by the file
  int virtMem = atoi(strtok(buff," "));
  virtMem = 1 << virtMem;
  int physMem = atoi(strtok(NULL, " "));
  physMem = 1 << physMem;
  int pageSize = atoi(strtok(NULL, " "));
  pageSize = 1 << pageSize;
  
  //Numframes is calculated by the ammount of Physical space that we have
  int numFrames = physMem/pageSize;
  
  //The number of pages is how many we can fit in the Virtual space.
  int numPages = virtMem/pageSize;
  int maxBits = ceil(sqrt(numPages));
  printf("INITIAL TABLE SETUP:\n");
  printf(" Virtual Memory Size: %i\n Physical Memory Size: %i\n Page Size: %i\n",
	 virtMem,physMem,pageSize);
  
  buff = (char*)malloc(sizeof(char*) * 255);
  
  //Given by the File
  int processes = atoi(strtok(fgets(buff,255,infile), " "));
  printf(" Number of Processes: %i\n Number of Pages: %i\n Number of Frames: %i\n",
	 processes,numPages,numFrames);
  
  
  
  // uint32_t pagetables[processes][numPages];
  uint32_t* pagetables = (uint32_t*)malloc(sizeof(uint32_t)  * numFrames);
  
  uint8_t ages[numFrames];

  //Init the ages
  for(int i =0; i<numFrames;i++) {
    ages[i] = 0;
  }
  
  for(int i=0;i<processes*numPages;i++) {
      pagetables[i] = 0;
  }

  //load in the frames 
  int div = numFrames/processes;


  int tmpFrameNum = 0;
  for(int j = 0; j<processes;j++) {
    for(int k = 0; k < div; k ++) {
      uint32_t page = 0;
      uint32_t frame = tmpFrameNum;
      ages[frame] = 0;
      ages[frame] = ~ages[frame];
      //Page number = k
      // frame = tmpFrameNum
      uint32_t proc = j;
      proc = proc << maxBits;
      page |= proc;
      page |= k;
      
      pagetables[tmpFrameNum] = page;
      tmpFrameNum++;
      
    }
  }
  
  printTable(pagetables,numPages,numFrames,ages);
  int comNum = 1;

  
  while(fgets(buff, 255, infile) != NULL) {
    

    //Comes from the command in the file.
    int tmpProc = atoi(strtok(buff, " "));
    char* tmpCom = strtok(NULL, " ");
    int tmpLoc = atoi(strtok(NULL, " "));
    unsigned offset = tmpLoc%pageSize;
    int tmpPage = tmpLoc/pageSize;
    printf("NEXT REFERENCE: Process %i requests %s%i\n",tmpProc,tmpCom,tmpLoc);
    bool loaded = false;
    unsigned frame = 0;
    
    for(int i = 0; i < numFrames;i++) {
      if(getProc(pagetables[i],maxBits) == tmpProc && getPage(pagetables[i],maxBits) == tmpPage) {
	loaded = true;
	frame = i;
      }
    }
    if(loaded) {
      unsigned physLoc = (frame*pageSize)+offset;
      printf("Page Number: %i\n",tmpPage);
      printf("Frame Number: %i\n",frame);
      printf("Physical Memory Location: %u\n",physLoc);
      
      setRef(&pagetables[frame]);
      
      if(strcmp(tmpCom,"w") == 0) {
	setMod(&pagetables[frame]);
      }

      //AGE:
      if(comNum%2 ==0) {
	inc_Age(pagetables,numPages,numFrames,processes,ages);
      }
      comNum++;


      printTable(pagetables,numPages,numFrames,ages);
    } else {
      
      uint32_t newPage = 0;
      uint32_t repFrame = findFrame(ages, numFrames,pagetables,numPages,processes,tmpProc,tmpPage);
     
      unsigned physLoc = (repFrame*pageSize)+offset;
      
      printf("Page Number: %i\n",tmpPage);
      printf("Frame Number: %i\n",repFrame);
      printf("Physical Memory Location: %u\n",physLoc);
      setRef(&newPage);
      
      
      if(strcmp(tmpCom, "w") == 0) {
	setMod(&newPage);
      }
      newPage = newPage|(tmpProc<<maxBits);
      newPage = newPage|tmpPage;
      pagetables[frame] = newPage;

      if(comNum%2 ==0) {
	inc_Age(pagetables,numPages,numFrames,processes,ages);
      }
      comNum++;

      printTable(pagetables,numPages,numFrames,ages);
    }
  }
}
