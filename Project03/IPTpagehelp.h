//IPTpagehelp.h
//Addison Thomas
//11 Nov. 2019
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

void bin_8(uint8_t n) 
{ 
  uint8_t i;
    for (i = 1 << 7; i > 0; i = i / 2) 
        (n & i)? printf("1"): printf("0");
}

void bin(uint32_t n) 
{ 
    uint32_t i; 
    for (i = 1 << 31; i > 0; i = i / 2) 
        (n & i)? printf("1"): printf("0");
}

uint32_t getRef(uint32_t page) {
  return ((page << 1) >> 31);
}

uint32_t setRef(uint32_t* page) {
  uint32_t tmp = 1 << 30;
  *page = *page|tmp;
  return *page;
}

uint32_t getMod(uint32_t page) {
  return (page>>31);
}

uint32_t setMod(uint32_t *page) {
  uint32_t tmp = 1 << 31;
  *page = *page|tmp;
  return *page;
}

uint32_t getPres(uint32_t page) {
  return ((page << 2) >> 31);
}

uint32_t setPres(uint32_t *page) {
  uint32_t tmp = 1 << 29;
  *page = *page|tmp;
  return *page;
}

uint32_t getFrame(uint32_t page) {
  uint32_t tmpPage = page;
  tmpPage = tmpPage << 4;
  tmpPage = tmpPage >> 4;
  return tmpPage;
}

uint32_t getProc(uint32_t page, int bits) {
  uint32_t tmp = page <<2;
  tmp = tmp >> 2;
  tmp = tmp >> bits;
  return tmp;
}

uint32_t getPage(uint32_t page, int bits) {
  int size = 32-bits;
  uint32_t tmp = page << size;
  tmp = tmp >> size;
  return tmp;
}

uint32_t findFrame(uint8_t *agearr, int size,uint32_t* pagetables,uint32_t numPages,int processes,uint32_t tmpProc,uint32_t tmpPage) {
  printf("PAGE FAULT\n");
  //Positions are used because vectors are not a thing in C

  int NRefModPos = -1;
  int RefNModPos = -1;
  int NRefNModPos = -1;
  int RefModPos = -1;
  //These arrays will hold the potentially valid Frames
 
  uint32_t NRefMod[size];
  uint32_t RefNMod[size];
  uint32_t NRefNMod[size];
  uint32_t RefMod[size];
  int ret = 0;

  //iterate over the frames
  for(int i = 0; i<size;i++) {
 
    uint32_t frame = pagetables[i];
      
    //if the frame is loaded, figure out what kind it is
     
    uint32_t ref = getRef(frame);
    uint32_t mod = getMod(frame);
    // not referenced, not modded (best case)
    if(ref == 0 && mod == 0) {
      NRefNModPos++;
      NRefNMod[NRefNModPos] = i;
      //Referenced, but not modded (Second best case)
    } else if(ref == 1 && mod == 0) {
      RefNModPos++;
      RefNMod[RefNModPos] = i;
      //Not referenced, but Modded (3rd best)
    } else if(ref == 0 && mod == 1) {
      NRefModPos++;
      NRefMod[NRefModPos] = i;
      //Referenced and Modified. Worst Case.
    } else if(ref == 1 && mod == 1) {
      RefModPos++;
      RefMod[RefModPos] = i;
    }
  }   
  
  if(NRefNModPos != -1) {
      uint8_t temp = 255;
      uint32_t ret = 0;
    for(int i =0;i<=NRefNModPos;i++) {
      int frame = NRefNMod[i];
     
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[frame];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

    uint32_t tmpMod = getMod(pagetables[ret]);
    if(tmpMod == 1) {
       printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
    }
    printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
    return ret;
  }

  if(RefNModPos != -1) {
    uint8_t temp = 255;
    uint32_t ret = 0;
    for(int i =0;i<=RefNModPos;i++) {
      int frame = RefNMod[i];
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[i];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

   uint32_t tmpMod = getMod(pagetables[ret]);
    if(tmpMod == 1) {
       printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
    }
    printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
    return ret;
  }

  if(NRefModPos != -1) {
    uint8_t temp = 255;
    uint32_t ret = 0;
    for(int i =0;i<=NRefModPos;i++) {
      int frame = NRefMod[i];
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[i];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

    uint32_t tmpMod = getMod(pagetables[ret]);
    if(tmpMod == 1) {
      printf("  Writing data from frame %i back to disk: page %i of process %i\n",
	     ret,tmpPage,tmpProc);
    }
    printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	   ret,tmpPage,tmpProc);
    return ret;
  }
  
   if(RefModPos != -1) {
      uint8_t temp = 255;
      uint32_t ret = 0;
    for(int i =0;i<=RefModPos;i++) {
      int frame = RefMod[i];
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[i];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

   uint32_t tmpMod = getMod(pagetables[ret]);
    if(tmpMod == 1) {
       printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
    }
    printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
    return ret;
  }
   
   ret = 0;
   agearr[ret] = 0;
   agearr[ret] = ~agearr[ret];

  uint32_t tmpMod = getMod(pagetables[ret]);
    if(tmpMod == 1) {
       printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
    }
    printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
   return ret;
  
}

void printTable(uint32_t* pagetables, uint32_t numPages,int numFrames,uint8_t* ages) {
  printf("Page Tables (with associated aging status):\n");
  int bits = ceil(sqrt(numPages));

   printf("     frame#:     mod     ref     proc#     page#     aging\n");
  for(int i = 0; i<numFrames;i++) {
    
      uint32_t frame = pagetables[i];
      printf("     %i:         %i       %i       %i        %i       ",
	     i,getMod(frame),getRef(frame),getProc(frame,bits),getPage(frame,bits));
      bin_8(ages[i]);
      printf("(%i)",ages[i]);
      printf("\n");
    }
}

void inc_Age(uint32_t* pagetables,uint32_t numPages,int numFrames, int processes,uint8_t* ages) {
  //age
  printf("Aging\n");
  for(int i = 0;i<numFrames;i++) {
    ages[i] = ages[i]>>1;
  }
      
  for(int i=0;i<numFrames;i++){
	 
    //  printf("GET REF at (%i,%i): %i)\n",i,j,getRef(pagetables[i][j]));
    uint32_t frame = i;
    // printf("FRAME %i",frame);
    uint32_t ref = getRef(pagetables[frame]);
    // printf("REFFF: %i",ref);
    ref = ref << 7;
    ages[frame] |= ref; 
    pagetables[frame] &= ~(1<<30);
  }
}
