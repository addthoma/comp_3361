//PT.c
//Addison Thomas
// 11 Nov. 2019
//
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "pagehelp.h"
#include <stdint.h>

int main(int argc, char* argv[]) {
  
  if(argc < 2) {
    printf("Incorrect number of inputs. Please include input filename\n");
    exit(0);
  }
  // Open the File
  FILE* infile = fopen(argv[1],"r");
  
  if(infile == NULL) {
    printf("Error opening file; exiting.\n");
    exit(0);
  }
  
  //Buffer to read into 
  char* buff = malloc(sizeof(char*) * 255);
  fgets(buff,255,infile);
  //Parameters given by the file
  int virtMem = atoi(strtok(buff," "));
  virtMem = 1 << virtMem;
  int physMem = atoi(strtok(NULL, " "));
  physMem = 1 << physMem;
  int pageSize = atoi(strtok(NULL, " "));
  pageSize = 1 << pageSize;
  
  //Numframes is calculated by the ammount of Physical space that we have
  int numFrames = physMem/pageSize;
  
  //The number of pages is how many we can fit in the Virtual space.
  int numPages = virtMem/pageSize;

  printf("INITIAL TABLE SETUP:\n");
  printf(" Virtual Memory Size: %i\n Physical Memory Size: %i\n Page Size: %i\n",
	 virtMem,physMem,pageSize);
  
  buff = (char*)malloc(sizeof(char*) * 255);
  
  //Given by the File
  int processes = atoi(strtok(fgets(buff,255,infile), " "));
  printf(" Number of Processes: %i\n Number of Pages: %i\n Number of Frames: %i\n",
	 processes,numPages,numFrames);
  
  
  
  // uint32_t pagetables[processes][numPages];
  uint32_t* pagetables = (uint32_t*)malloc(sizeof(uint32_t) * processes * numPages);
  
  uint8_t ages[numFrames];

  //Init the ages
  for(int i =0; i<numFrames;i++) {
    ages[i] = 0;
  }
  
  for(int i=0;i<processes*numPages;i++) {
      pagetables[i] = 0;
  }

  //load in the frames 
  int div = numFrames/processes;

  int tmpFrameNum = 0;
  for(int j = 0; j<processes;j++) {
    for(int k = 0; k < div; k ++) {
      uint32_t page = 0;
      uint32_t frame = tmpFrameNum;
      ages[frame] = 0;
      ages[frame] = ~ages[frame];
      setPres(&page);
      page |= frame;
      tmpFrameNum++;
      pagetables[(j*numPages)+k] = page;
    }
  }
  
  printTable(pagetables,numPages,processes,ages);
  int comNum = 1;

  
  while(fgets(buff, 255, infile) != NULL) {
    

    //Comes from the command in the file.
    int tmpProc = atoi(strtok(buff, " "));
    char* tmpCom = strtok(NULL, " ");
    int tmpLoc = atoi(strtok(NULL, " "));
    unsigned offset = tmpLoc%pageSize;
    int tmpPage = tmpLoc/pageSize;
    printf("NEXT REFERENCE: Process %i requests %s%i\n",tmpProc,tmpCom,tmpLoc);
    
    if(getPres(pagetables[(tmpProc*numPages)+tmpPage])==1) {
      unsigned frame = getFrame(pagetables[(tmpProc*numPages)+tmpPage]);
     
      unsigned physLoc = (frame*pageSize)+offset;
      printf("Page Number: %i\n",tmpPage);
      printf("Frame Number: %i\n",frame);
      printf("Physical Memory Location: %u\n",physLoc);
      
      setRef(&pagetables[(tmpProc*numPages)+tmpPage]);
      
      if(strcmp(tmpCom,"w") == 0) {
	setMod(&pagetables[(tmpProc*numPages)+tmpPage]);
      }

      //AGE:
      if(comNum%2 ==0) {
	inc_Age(pagetables,numPages,numFrames,processes,ages);
      }
      comNum++;


      printTable(pagetables,numPages,processes,ages);
    } else {
      uint32_t newPage = 0;
      uint32_t repFrame = findFrame(ages, numFrames,pagetables,numPages,processes,tmpProc,tmpPage);
      unsigned physLoc = (repFrame*pageSize)+offset;
      
      printf("Page Number: %i\n",tmpPage);
      printf("Frame Number: %i\n",repFrame);
      printf("Physical Memory Location: %u\n",physLoc);
      setRef(&newPage);
      setPres(&newPage);
      
      if(strcmp(tmpCom, "w") == 0) {
	setMod(&newPage);
      }
      newPage = newPage|repFrame;
      
      pagetables[(tmpProc*numPages)+tmpPage] = newPage;

      if(comNum%2 ==0) {
	inc_Age(pagetables,numPages,numFrames,processes,ages);
      }
      comNum++;

      printTable(pagetables,numPages,processes,ages);
    }
  }
}
