PT and IPT (Page table and inverted page table) are two separate programs
that simulate a page table system.


USE: running these programs just takes in one parameter for a file name 
	consisting of a series of commands that follow the structure:

[bits of virtual mem] [bits of phys mem] [bits of page size]
[processes]
commands: [process] ['r' or 'w'] [virt mem location]

EX:

9 7 5
1
0 r 20
0 w 40
