//pagehelp.h
//Addison Thomas
//11 Nov. 2019
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

void bin_8(uint8_t n) 
{ 
  uint8_t i;
    for (i = 1 << 7; i > 0; i = i / 2) 
        (n & i)? printf("1"): printf("0");
}

void bin(uint32_t n) 
{ 
    uint32_t i; 
    for (i = 1 << 31; i > 0; i = i / 2) 
        (n & i)? printf("1"): printf("0");
}

uint32_t getRef(uint32_t page) {
  return ((page << 1) >> 31);
}

uint32_t setRef(uint32_t* page) {
  uint32_t tmp = 1 << 30;
  *page = *page|tmp;
  return *page;
}

uint32_t getMod(uint32_t page) {
  return (page>>31);
}

uint32_t setMod(uint32_t *page) {
  uint32_t tmp = 1 << 31;
  *page = *page|tmp;
  return *page;
}

uint32_t getPres(uint32_t page) {
  return ((page << 2) >> 31);
}

uint32_t setPres(uint32_t *page) {
  uint32_t tmp = 1 << 29;
  *page = *page|tmp;
  return *page;
}

uint32_t getFrame(uint32_t page) {
  uint32_t tmpPage = page;
  tmpPage = tmpPage << 4;
  tmpPage = tmpPage >> 4;
  return tmpPage;
}

uint32_t findFrame(uint8_t *agearr, int size,uint32_t* pagetables,uint32_t numPages,int processes,uint32_t tmpProc,uint32_t tmpPage) {
  printf("  PAGE FAULT...\n");
  //Positions are used because vectors are not a thing in C
  int notPresPos = -1;
  int NRefModPos = -1;
  int RefNModPos = -1;
  int NRefNModPos = -1;
  int RefModPos = -1;
  //These arrays will hold the potentially valid Frames
  uint32_t* NPres = (uint32_t*)malloc(sizeof(uint32_t*)*size);
  uint32_t* NRefMod = (uint32_t*)malloc(sizeof(uint32_t)*size);
  uint32_t* RefNMod = (uint32_t*)malloc(sizeof(uint32_t)*size);
  uint32_t* NRefNMod = (uint32_t*)malloc(sizeof(uint32_t)*size);
  uint32_t* RefMod = (uint32_t*)malloc(sizeof(uint32_t)*size);
  int ret = 0;
 
  //iterate over the frames
  for(int i = 0; i<size;i++) {
    //if it isnt present we can return that one
    bool pres = false;
    // iterate over the page tables
    for(int j = 0;j<processes*numPages;j++){
     
      uint32_t page = pagetables[j];
      
      //if the frame is loaded, figure out what kind it is
      if(getFrame(page) == i && getPres(page) == 1) {
	pres = true;
	uint32_t ref = getRef(page);
	uint32_t mod = getMod(page);
	// not referenced, not modded (best case)
	if(ref == 0 && mod == 0) {
	  NRefNModPos++;
	  NRefNMod[NRefNModPos] = i;
	  //Referenced, but not modded (Second best case)
	} else if(ref == 1 && mod == 0) {
	  RefNModPos++;
	  RefNMod[RefNModPos] = i;
	  //Not referenced, but Modded (3rd best)
	} else if(ref == 0 && mod == 1) {
	  NRefModPos++;
	  NRefMod[NRefModPos] = i;
	  //Referenced and Modified. Worst Case.
	} else if(ref == 1 && mod == 1) {
	  RefModPos++;
	  RefMod[RefModPos] = i;
	}
      }
    }

    
    if(pres == false) {
      ret = i;
      agearr[ret] = 0;
      agearr[ret] = ~agearr[ret];
     
      for(int i=0;i<processes*numPages;i++) {
	if(getFrame(pagetables[i]) == ret  && getPres(pagetables[i]) == 1) {
	  uint32_t tmpMod = getMod(pagetables[i]);
	  if(tmpMod == 1) {
	    printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		   ret,tmpPage,tmpProc);
	  }
	  printf("  Loading new data into frame %i from disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
	  pagetables[i] = 0;
	}
      }
  
      return ret;
    }
  }

  if(NRefNModPos != -1) {
      uint8_t temp = 255;
      uint32_t ret = 0;
    for(int i =0;i<=NRefNModPos;i++) {
      int frame = NRefNMod[i];
     
      if(agearr[frame] < temp) {
        
	ret = frame;
	temp = agearr[frame];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

    for(int i=0;i<processes*numPages;i++) {
      if(getFrame(pagetables[i]) == ret  && getPres(pagetables[i]) == 1) {
	uint32_t tmpMod = getMod(pagetables[i]);
	if(tmpMod == 1) {
	  printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
	}
	printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
	pagetables[i] = 0;
      }
    }
    return ret;
  }

  if(RefNModPos != -1) {
    uint8_t temp = 255;
    uint32_t ret = 0;
    for(int i =0;i<=RefNModPos;i++) {
      int frame = RefNMod[i];
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[i];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

    for(int i=0;i<processes*numPages;i++) {
      if(getFrame(pagetables[i]) == ret  && getPres(pagetables[i]) == 1) {
	uint32_t tmpMod = getMod(pagetables[i]);
	if(tmpMod == 1) {
	  printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
	}
	printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
	pagetables[i] = 0;
      }
    }
    return ret;
  }

  if(NRefModPos != -1) {
    uint8_t temp = 255;
    uint32_t ret = 0;
    for(int i =0;i<=NRefModPos;i++) {
      int frame = NRefMod[i];
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[i];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

    for(int i=0;i<processes*numPages;i++) {
      if(getFrame(pagetables[i]) == ret  && getPres(pagetables[i]) == 1) {
	uint32_t tmpMod = getMod(pagetables[i]);
	if(tmpMod == 1) {
	  printf("  Writing data from frame %i back to disk: page %i of process %i",
		 ret,tmpPage,tmpProc);
	}
	printf("  Loading new data into frame %i from disk: page %i of process %i",
	       ret,tmpPage,tmpProc);
	pagetables[i] = 0;
      }
    }
    return ret;
  }
  
   if(RefModPos != -1) {
      uint8_t temp = 255;
      uint32_t ret = 0;
    for(int i =0;i<=RefModPos;i++) {
      int frame = RefMod[i];
      if(agearr[frame] < temp) {
	ret = frame;
	temp = agearr[i];
      }
    }
    agearr[ret] = 0;
    agearr[ret] = ~agearr[ret];

    for(int i=0;i<processes*numPages;i++) {
      if(getFrame(pagetables[i]) == ret && getPres(pagetables[i]) == 1) {
	uint32_t tmpMod = getMod(pagetables[i]);
	if(tmpMod == 1) {
	  printf("  Writing data from frame %i back to disk: page %i of process %i\n",
		 ret,tmpPage,tmpProc);
	}
	printf("  Loading new data into frame %i from disk: page %i of process %i\n",
	       ret,tmpPage,tmpProc);
	pagetables[i] = 0;
      }
    }
    return ret;
  }
   
   ret = 0;
   agearr[ret] = 0;
   agearr[ret] = ~agearr[ret];

   for(int i=0;i<processes*numPages;i++) {
     if(getFrame(pagetables[i]) == ret && getPres(pagetables[i]) == 1) {
       	uint32_t tmpMod = getMod(pagetables[i]);
	if(tmpMod == 1) {
	  printf("  Writing data from frame %i back to disk: page %i of process %i",
		 ret,tmpPage,tmpProc);
	}
	printf("  Loading new data into frame %i from disk: page %i of process %i",
	       ret,tmpPage,tmpProc);
       pagetables[i] = 0;
     }
   }
   return ret;
  
}

void printTable(uint32_t* pagetables, uint32_t numPages,int processes,uint8_t* ages) {
  printf("Page Tables (with associated aging status):\n");
  for(int i = 0; i<processes;i++) {
    printf("Process %i\n",i);
    printf("     page#:     mod     ref     pres     frame#     aging\n");
    for(int j = 0; j <numPages;j++) {
      uint32_t page = pagetables[(i*numPages)+j];
      printf("     %i:         %i       %i       %i        %i          ",
	     j,getMod(page),getRef(page),getPres(page),getFrame(page));

      if(getPres(page) == 1) {
	bin_8(ages[getFrame(page)]);
	printf(" (%i)",ages[getFrame(page)]);
      }
      printf("\n");
    }
  }
}

void inc_Age(uint32_t* pagetables,uint32_t numPages,int numFrames, int processes,uint8_t* ages) {
  	//age
	printf("Aging\n");
	for(int i = 0;i<numFrames;i++) {
	  ages[i] = ages[i]>>1;
	}
      
	for(int i=0;i<processes;i++){
	  for(int j=0;j<numPages;j++) {
	    //  printf("GET REF at (%i,%i): %i)\n",i,j,getRef(pagetables[i][j]));
	    if(getPres(pagetables[(i*numPages)+j])==1) {
	      uint32_t frame = getFrame(pagetables[(i*numPages)+j]);
	      // printf("FRAME %i",frame);
	      uint32_t ref = getRef(pagetables[(i*numPages)+j]);
	      // printf("REFFF: %i",ref);
	      ref = ref << 7;
	      ages[frame] |= ref;
	    }
	    pagetables[(i*numPages)+j] &= ~(1<<30);
	  }
	}
}
